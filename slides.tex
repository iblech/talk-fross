\documentclass[12pt,handout,compress,notheorems,ngerman,utf8,t]{beamer}
\usepackage{etex}

\usepackage[ngerman]{babel}
\usepackage[export]{adjustbox}
\usepackage{multicol,tikz,ragged2e}
\hypersetup{colorlinks=true}
\usepackage[protrusion=true,expansion=true]{microtype}
\usetikzlibrary{mindmap,shadows}

\graphicspath{{images/}}

\DeclareSymbolFont{extraup}{U}{zavm}{m}{n}
\DeclareMathSymbol{\varheart}{\mathalpha}{extraup}{86}
\DeclareMathSymbol{\vardiamond}{\mathalpha}{extraup}{87}

%\usetheme{Warsaw}
\useinnertheme[shadow=true]{rounded}
\useoutertheme{split}
\usecolortheme{orchid}
\usecolortheme{whale}
\setbeamerfont{block title}{size={}}

\useinnertheme{rectangles}

\usecolortheme{seahorse}
\definecolor{mypurple}{RGB}{150,0,255}
%\setbeamercolor{structure}{fg=mypurple}

\usefonttheme{serif}
\usepackage[T1]{fontenc}
\usepackage{libertine}

\newcommand{\imgslide}[1]{{\usebackgroundtemplate{\parbox[c][\paperheight][c]{\paperwidth}{\centering\includegraphics[width=\paperwidth]{#1}}}\begin{frame}[plain]\end{frame}}}

\setbeamertemplate{navigation symbols}{}

\setbeamertemplate{title page}[default][colsep=-1bp,rounded=false,shadow=false]
\setbeamertemplate{frametitle}[default][colsep=-2bp,rounded=false,shadow=false,center]

\newcommand{\hil}[1]{{\usebeamercolor[fg]{item}{\textbf{#1}}}}

\setbeamertemplate{frametitle}{%
  \vskip0.7em%
  \leavevmode%
  \begin{beamercolorbox}[dp=1ex,center]{}%
    \usebeamercolor[fg]{item}{\textbf{\Large \insertframetitle}}
  \end{beamercolorbox}%
  \vspace*{-0.2em}%
}

\setbeamertemplate{footline}{%
  \begin{beamercolorbox}[wd=\paperwidth,ht=2.25ex,dp=1ex,right,rightskip=1mm,leftskip=1mm]{}%
    \usebeamerfont{date in head/foot}
    Freie Software und Creative Commons \hfill
    \insertframenumber\,/\,\inserttotalframenumber
  \end{beamercolorbox}%
  \vskip0pt%
}

\newcommand{\backupstart}{
  \newcounter{framenumberpreappendix}
  \setcounter{framenumberpreappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
  \addtocounter{framenumberpreappendix}{-\value{framenumber}}
  \addtocounter{framenumber}{\value{framenumberpreappendix}}
}

\title{\large Freie Software und Creative Commons \\
  \normalsize Gemeingüter schaffen, nutzen und schützen}
\author[Ingo Blechschmidt]{
  \scriptsize \vspace*{-2em} \\
  Augsburger Zukunftstagung am 24. Februar 2018 \\
  \scalebox{0.9}{Ingo Blechschmidt} \\
  \bigskip
  \textbf{Willkommen bei unserem gemeinsamen Workshop! \\ Ein möglicher Ablauf:} \\
  \medskip
  \
  {\renewcommand{\insertenumlabel}{1}\usebeamertemplate{enumerate item}} Meta \quad
  {\renewcommand{\insertenumlabel}{2}\usebeamertemplate{enumerate item}} Freie Software \quad
  {\renewcommand{\insertenumlabel}{3}\usebeamertemplate{enumerate item}} Weitere Gemeingüter \\
  \vspace*{20em}
}
\date{24. Februar 2018}

\newenvironment{noteframe}[1]{
  \begingroup
  \setbeamercolor{background canvas}{bg=black!10}
  \addtocounter{framenumber}{-1}
  \begin{frame}{#1}\small\justifying}{%
  \end{frame}\endgroup%
}

\begin{document}

{\usebackgroundtemplate{\begin{minipage}{\paperwidth}\vspace*{3.35cm}\includegraphics[width=\paperwidth]{freedom}
\\ \centering \tiny Foto: \href{https://www.flickr.com/photos/steinliland/14435047450/}{Stein Liland} (CC-BY-NC) \end{minipage}}
\frame{\titlepage}}


\section{Meta}

\begin{noteframe}{Lizenz}
  \justifying
  Dieser Foliensatz steht unter der Creative-Commons-BY-SA-Lizenz. Das heißt,
  dass du diesen Foliensatz nach Belieben mit anderen teilen darfst. Du kannst
  auch gerne \href{https://gitlab.com/iblech/talk-fross}{die Quelldateien
  herunterladen} und die Folien verändern. Du könntest sie sogar verwenden, um
  einen öffentlichen Vortrag zu halten und damit Geld zu verdienen.
  \medskip

  Einzige Bedingung: Du musst meinen Namen nennen ("`BY"') und etwaige
  Veränderungen mit den gleichen Rechten weitergeben ("`SA"'). Das
  hypothetische Publikum deines Vortrags hat also dieselben Möglichkeiten wie
  du selbst sie mit diesem Foliensatz hast.
  \medskip

  Dieser Foliensatz wurde übrigens unter ausschließlicher Verwendung freier
  Software erstellt.
\end{noteframe}

\begin{frame}{Inhalte}
  \vspace*{-3em}
  \hspace*{-1.8em}
  \scalebox{0.7}{\begin{tikzpicture}[every annotation/.style = {draw, fill = white, font = \large}]
    \path[
      mindmap,
      concept color=black!40,
      text=white,
      every node/.style={concept},
      root/.style={concept color=black!40, font=\large\bfseries,text width=9em},
      level 1 concept/.append style={font=\large\bfseries, sibling angle=60,text width=5.7em, level distance=11em,inner sep=0pt},
      level 2 concept/.append style={font=\bfseries,level distance=9em,sibling angle=40,text width=5.4em},
    ]
    node[root] {Gemeingüter} [counterclockwise from=180]
      child[concept color=blue!60] {
        node {Freie Software} [counterclockwise from=140]
          child { node {Was ist das?} }
          child { node {Wer macht das?} }
          child { node {Was hilft mir das?} }
      }
      child[concept color=red!60] {
        node {Freie Medien} [counterclockwise from=200]
          child { node {Freie Texte} }
          child { node {Freie Fotos \& Zeichnungen} }
          child { node {Freie Musik} }
      }
      child[concept color=purple!60] {
        node {Freie Daten} [counterclockwise from=260]
          child { node {Wiss. Daten} }
          child { node {Karten} }
          child { node {Haushalt} }
      }
      child[concept color=orange!60] { node {Freies Saatgut} };
  \end{tikzpicture}}
\end{frame}


\section{Freie Software}

\newcommand{\icon}[1]{\includegraphics[height=0.9cm]{#1}}

\begin{frame}{Was ist freie Software? \\[-0.3em] \normalsize Programme, Apps}
  \vspace*{-0.5em}
  \begin{columns}
    \begin{column}[t]{0.5\textwidth}
      \hil{Freie} Software darf man \ldots
      \begin{enumerate}
        \item zu jedem Zweck ausführen,
        \item untersuchen und verändern,
        \item verbreiten und
        \item verbessert verbreiten.
      \end{enumerate}

      Sie ist \hil{meist kostenlos}.
      \bigskip

      \centering

      \icon{tux}
      \icon{firefox}
      \icon{thunderbird}
      \icon{libreoffice} \\[0.2em]
      \icon{signal}
      \icon{vlc}
      \icon{osmand}
      \icon{gimp}
    \end{column}

    \hspace*{-1.4em}
    \vrule
    \vrule
    \vrule
    \vrule
    \hspace*{0.7em}

    \begin{column}{0.55\textwidth}
      \hil{Proprietäre} Software darf man \ldots
      \begin{enumerate}
        \item oft nur für bestimmte Zwecke ausführen,
        \item nicht untersuchen oder verändern,
        \item nicht verbreiten und
        \item nicht verbessert verbreiten.
      \end{enumerate}

      Sie ist meist \hil{nicht kostenlos}.
      \bigskip

      \centering
      \icon{microsoft}
      \icon{ie}
      \icon{outlook}
      \icon{ms-office} \\[0.2em]
      \icon{whatsapp}
      \icon{acrobat}
      \icon{google}
      \icon{photoshop}
    \end{column}
  \end{columns}
\end{frame}

\newcommand{\portraitz}[3]{{\includegraphics[height=3cm]{#1}\\\small #2\\\tiny #3\par}}
\newcommand{\portrait}[3]{\begin{column}{0.25\textwidth}\centering\portraitz{#1}{#2}{#3}\end{column}}

\begin{frame}{Wer entwickelt freie Software?}
  \begin{columns}
    \portrait{richard-stallman}{Richard Stallman}{Foto: Dkoukoul (CC-BY-SA)}
    \portrait{audrey-tang}{Audrey Tang}{Foto: daisuke1230 (CC-BY)}
    \portrait{sva}{sva}{Foto: CCC (CC-BY)}
    \hspace*{-1em}
    \visible<3>{\portrait{hey-you}{Mitmachen!}{Zeichnung: oksmith (CC0)}}
  \end{columns}
  \bigskip

  \begin{columns}[T]
    \visible<2->{\begin{column}{0.1\textwidth}
      \raggedleft
      \icon{ubuntu} \\
      \icon{redhat} \\
      \icon{suse}
    \end{column}}

    \hspace*{-1.5em}

    \begin{column}{0.85\textwidth}
      % https://www.cs.columbia.edu/~w3157/hackathon/img/peace.jpg
      % https://www.cs.columbia.edu/~w3157/hackathon/img/2017_spring_photos/large-19.jpg
      \centering
      \includegraphics[height=2.6cm]{hackathon-1}
      \quad
      \includegraphics[height=2.6cm]{hackathon-2} \\
      \small Teilnehmende eines Hackathons in New York \\
      \tiny Foto: \href{https://www.cs.columbia.edu/~w3157/hackathon/}{Jae Woo Lee} (CC-BY-SA)\par
    \end{column}

    \hspace*{-1.5em}

    \visible<2->{\begin{column}{0.05\textwidth}
      \icon{google} \\
      \icon{microsoft} \\
      \icon{intel}
    \end{column}}
  \end{columns}
\end{frame}

\begin{noteframe}{Praktische Aspekte}
  \begin{itemize}\justifying
    \item Freie Software ist von hoher Qualität und fast immer kostenlos.
    \item Linux läuft oft auch auf älteren Computern flott.
    \item Linux kommt mit einer umfangreichen Sammlung freier Programme für
    Internet, Büro, Multimedia und Bildung. Es ist nicht nötig,
    sich Programme von diversen Webseiten manuell einzusammeln.
    \item Viren und Würmer sind für Linux eine akademische Kuriosität.
    \item Wie bei Windows gibt es Sicherheitsaktualisierungen. Anders als bei
    Windows kommen wichtige Aktualisierungen so schnell wie möglich, nicht nach
    langer Wartezeit, und anders als bei Windows kann man den Computer
    problemlos benutzen, während Aktualisierungen eingespielt werden.
    \item Linux bietet für alle, die das wollen, mehr Flexibilität. Unter Linux
    fällt der Einstieg in die Programmierung viel leichter.
  \end{itemize}
\end{noteframe}

\begin{noteframe}{Freie Software und Transparenz}
  Bei freier Software können unabhängige Privatpersonen und Institutionen
  überprüfen, ob die Software wirklich genau das macht, was sie verspricht. Das
  ist bei proprietärer Software nicht möglich.

  \begin{itemize}\justifying
    \item WhatsApp und andere Kommunikationsprogramme behaupten, alle
    Nachrichten verschlüsselt zu übertragen. Ob unabsichtliche
    Programmierfehler oder absichtliche Hintertüren in WhatsApp vorhanden sind,
    können wir nicht ausschließen.

    Eine freie Alternative zu WhatsApp ist Signal.
    \item Den Abgasskandal hätte es nicht gegeben, wenn Autohersteller
    verpflichtet wären, die Steuerung ihrer Motoren offen zu legen.
    \item Über denselben Kanal, mit dem normale Handys mit sinnvollen
    Aktualisierungen versorgt werden, können die Hersteller auch Programme
    einschleusen, die unbemerkt alle Gespräche mitschneiden und übertragen.
  \end{itemize}
\end{noteframe}

\begin{noteframe}{Herausforderungen bei freier Software}
  \begin{itemize}\justifying
    \item Zu manchen spezialisierten Aufgabenbereichen gibt es noch keine freie
    Software.
    \item Wenn man von Windows auf Linux wechselt, muss man sich etwas
    umgewöhnen.
    \item Es wäre gut, wenn unabhängige Programmiererinnen und Programmierer
    von der Entwicklung freier Software leben könnten. Da freier Software auch
    eine Infrastrukturrolle zukommt (Privatpersonen, Vereine, Firmen und
    Gemeinden können auf freier Software aufbauen), könnte auch der Staat einen
    Beitrag zur Finanzierung liefern.

    In Deutschland gibt es schon Ansätze in diese Richtung, etwa den vom
    BMBF geförderten \href{https://prototypefund.de/}{Prototype Fund}.

    Wir könnten Mikrozahlungen vorantreiben und so Crowdfunding
    verbessern.
  \end{itemize}
\end{noteframe}

\begin{noteframe}{Linux ausprobieren}
  Ich empfehle für Windows-Umsteigerinnen und -Umsteiger Linux in der Variante
  \href{https://linuxmint.com/}{Linux Mint}.
  \href{https://de.wikihow.com/Linux-Mint-installieren}{Im Internet gibt es
  Anleitungen,} die erklären, wie man Linux Mint installiert.
  \medskip

  Leichter ist es aber, mit dem eigenen Laptop zu einem der monatlichen Treffen
  der \href{https://www.luga.de/}{Linux User Group Augsburg} zu kommen. Eine
  kurze Anmeldung per E-Mail genügt; die Mitglieder der LUGA freuen sich,
  ehrenamtlich Linux vorzuführen oder parallel neben einem bereits vorhandenen
  Windows zu installieren. Es gibt auch die Möglichkeit, Linux auf einem
  USB-Stick auszuprobieren. Sobald man den wieder absteckt, ist alles beim
  Alten. So kann man Linux ganz einfach kennenlernen.
  \medskip

  Die LUGA organisiert seit vielen Jahren
  \href{https://www.luga.de/Aktionen/LIT-2018/}{Linux-Infotage}. Der
  diesjährige wird am 21. April 2018 stattfinden. Alle Interessierten sind
  herzlich eingeladen.
\end{noteframe}


\section{Weitere Gemeingüter}

\begin{frame}{Creative Commons}
  \vspace*{-1em}
  \centering
  \hil{Urheberrecht ausnutzen, um Freiheit zu schützen.}
  \bigskip

  \includegraphics[width=0.4\textwidth]{cc-options}
  \par
  \bigskip

  \raggedright

  Nutzung und Weitergabe erlaubt, aber \ldots
  \begin{tabbing}
    \hil{NC:} \= \kill
    \hil{BY:} \> Namensnennung erforderlich \\
    \hil{SA:} \> Weitergabe von Veränderungen unter gleichen Rechten \\
    \hil{NC:} \> nicht für kommerzielle Zwecke (Achtung, missverständlich) \\
    \hil{ND:} \> keine Veränderung erlaubt
  \end{tabbing}
  Persönliche Empfehlung: CC-BY-SA
\end{frame}


\subsection{Freie Texte}

\begin{frame}{Freie Texte}
  \centering
  \hil{Bekanntheit} \textbullet{}
  \hil{Übersetzung} \textbullet{}
  \hil{Fan Fiction} \textbullet{}
  \hil{Verbesserung}
  \par
  \bigskip

  \begin{columns}
    \begin{column}{0.4\textwidth}
      \centering
      \portraitz{cory-doctorow}{Cory Doctorow}{Foto: Ed Schipul (CC-BY-SA)}
      \par
      \bigskip

      Viele seiner Bücher unter CC-BY-SA-NC.

      "`Copyright is failing to serve artists."'
    \end{column}
    % Verleihtradition erwähnen!

    \begin{column}{0.4\textwidth}
      \centering
      \portraitz{wikipedia}{Wikipedia}{}
      \par
      \bigskip \ \\[-0.6em]

      Alle Inhalte unter CC-BY-SA.
    \end{column}
  \end{columns}
\end{frame}


\subsection{Freie Musik und freie Fotos}

\begin{frame}{Freie Musik und freie Fotos}
  \centering

  \hil{Bekanntheit} \textbullet{}
  \hil{Remixes} \textbullet{}
  \hil{Cover} \textbullet{}
  \hil{Liedtextvideos}
  \bigskip

  \begin{columns}
    \begin{column}{0.5\textwidth}
      \centering
      \portraitz{jonathan-coulton}{Jonathan Coulton}{Foto: Vivian Jayant (CC-BY)}
      \bigskip

      Wählt CC-BY-NC für seine Lieder.
    \end{column}

    \begin{column}{0.5\textwidth}
      \centering
      \portraitz{samuel-zeller}{Samuel Zeller}{Foto: Samuel Zeller (CC0)}
      \par
      \bigskip

      Wählt CC0 für seine Fotos.
    \end{column}
  \end{columns}
\end{frame}


\subsection{Freie Daten}

\begin{frame}{Freie Daten}
  \centering
  \includegraphics[height=2cm]{wikidata}\qquad
  \includegraphics[height=2cm]{openstreetmap}
  \bigskip
  \bigskip

  \includegraphics[height=2cm]{g0v-tw}\qquad
  \includegraphics[height=2cm]{bundeshaushalt}\qquad
  \includegraphics[height=2cm]{govdata}
  \bigskip
  \bigskip

  \includegraphics[height=2cm]{azimuth}\qquad
  \includegraphics[height=2cm]{luftdaten}
\end{frame}

\begin{noteframe}{Ein restriktives Extrembeispiel}
  \justifying

  Anna stellt eine ihrer Kurzgeschichten auf ihrer privaten Webseite
  online. Sie möchte, dass ihre Freundinnen und Freunde ihre Geschichte lesen
  können.
  \medskip

  Sie möchte aber nicht, dass ein Verlag ohne ihr Einverständnis ihre
  Geschichte druckt und damit Geld verdient, und sie möchte auch nicht, dass
  jemand in Form einer Fan Fiction ihre Geschichte weiterdenkt. Niemand soll
  Übersetzungen anfertigen und keine Lehrkraft soll sie in
  ein Arbeitsblatt integrieren.
  \medskip

  In diesem Fall muss Anna \hil{nichts weiter tun}. Das Urheberrecht
  verbietet jedwede Nutzung ohne Annas Einverständnis. Nach derzeitigem
  deutschem Recht gilt dieser Schutz bis 70 Jahre nach Annas Tod. Danach ist
  ihre Geschichte gemeinfrei, sodass alle mit ihrer Geschichte machen können,
  was sie wollen. Ohne Lobbyarbeit beim Gesetzgeber können Anna und ihre
  Nachfahren daran nichts ändern.
\end{noteframe}

\begin{noteframe}{Ein weiteres Extrembeispiel}
  \justifying
  Tom nimmt einen selbst geschriebenen Song auf und veröffentlicht ihn
  auf einem sozialen Netzwerk seiner Wahl. Wer möchte, soll seinen Song gerne
  anhören.
  \medskip

  Tom möchte aber nicht, dass jemand seinen Song öffentlich abspielt oder
  seinen Song covert und damit Geld verdient. Er möchte noch nicht einmal,
  dass ein Fan den Song nachsingt und die Neuinterpretation ganz ohne
  kommerzielle Hintergedanken auf YouTube stellt. Tom möchte seinen Fans
  sogar verbieten, Videos hochzuladen, die zu Toms unveränderter Aufnahme den
  Liedtext einblenden.
  \medskip

  In diesem Fall muss Tom \hil{nichts weiter tun}. Das Urheberrecht gestattet
  keine Nutzungen dieser Art ohne Toms explizites Einverständnis.
\end{noteframe}

\begin{noteframe}{Extrem in die andere Richtung}
  \justifying\small
  Jennifer schießt ein besonders emotionales Foto, das das Bienensterben
  dokumentiert. Jennifer ist nur wichtig, dass ihr Schutzappell möglichst
  großen Anklang findet. Sie freut sich daher, wenn es möglichst breit geteilt
  wird. Ihr ist nicht wichtig, ob dabei ihr Name genannt wird, und sie hat auch
  kein Problem damit, wenn jemand ihr Foto in einem Dokumentarfilm einbindet
  und Geld für den Film verlangt.
  \medskip

  In diesem Fall muss Jennifer ihr Foto \hil{unter CC0 lizenzieren}. Das hat im
  Wesentlichen dieselben Konsequenzen, wie wenn ihr Foto nicht unter
  Urheberrecht stünde.
  \medskip

  Wenn Jennifer dagegen die CC0-Deklaration vergisst, kann niemand ihr Foto in
  ihrem Sinne verwenden. Umweltschutzorganisationen und Lehrkräfte könnten zwar
  immer noch Jennifer kontaktieren und um Verwendungserlaubnis bitten, die
  sie dann erteilen würde. In der Praxis wird das aber kaum jemand machen, da
  viele nicht die Zeit haben, Fotografierende anzuschreiben und ihre Antworten
  abzuwarten.
\end{noteframe}

\begin{noteframe}{Ein reales Beispiel: Jonathan Coulton}
  Jonathan Coulton ist ein US-amerikanischer Singer-Songwriter. Er freut sich,
  wenn seine Fans seine Musik neu interpretieren und Cover-Videos auf YouTube
  stellen oder gar eigene Musikvideos zu seinen Songs anfertigen. Jonathan
  freut sich auch, wenn seine Lieder auf nicht-kommerziellen Veranstaltungen
  gespielt werden. Er möchte aber nicht, dass jemand mit seiner Musik ohne
  Einverständnis Geld verdient, und außerdem ist ihm wichtig, dass stets sein
  Name genannt wird.
  \medskip

  Jonathan stellt seine Songs daher unter die \hil{CC-BY-NC-Lizenz}. Dies führt
  dazu, dass seine Lieder in Sammlungen von CC-Musik aufgenommen werden. Manche
  Linux-Distributionen liefern Jonathans Musik gleich mit, sodass sie an
  Zehntausende verteilt werden. Er gewinnt so Fans aus aller Welt, die ihn über
  viele Jahre hinweg verfolgen, auf seine Konzerte gehen und um ihn zu
  unterstützen seine Lieder auch kaufen. Er muss seine Einnahmen mit keiner
  Plattenfirma teilen.
\end{noteframe}

\begin{noteframe}{Ein reales Beispiel: Jonathan Coulton}
  Jonathan schreibt auf seiner Webseite: "`All I can say is that Creative
  Commons is the most powerful idea I’ve heard since they told me there was
  going to be a sequel to Star Wars. Everyone in the world should read Lawrence
  Lessig’s book Free Culture. I saw him speak about CC at PopTech 2003 and I
  was so excited by it that I nearly wet my pants. The things he says make so
  much sense, and yet they’re so counter to the current thinking about
  intellectual property – it makes you want to, well, wet your pants."'
  \medskip

  \href{https://www.youtube.com/watch?v=1gaqHi6--U8}{Kostprobe (von einem Fan
  erstelltes Video)}
\end{noteframe}

\begin{noteframe}{Ein reales Beispiel: Cory Doctorow}
  Cory Doctorow ist ein bekannter Science-Fiction-Autor. Er freut sich, wenn
  viele seine Romane lesen und zum Nachdenken angeregt werden. Wenn sich seine Fans
  so sehr mit seinen Geschichten auseinandersetzen, dass sie sogar Fan Fiction
  anfertigen oder sie zu Theaterstücken adaptieren, ist er besonders glücklich.
  Er möchte aber nicht, dass jemand mit seinen Texten Geld verdient, und er
  möchte auch nicht, dass abgeleitete Werke wie Übersetzungen oder Fan Fictions
  nicht wieder dieselbe Freiheit genießen.
  \medskip

  Cory veröffentlicht seine Romane daher unter der \hil{CC-BY-SA-NC-Lizenz}. Er
  macht immer und immer wieder die Erfahrung, dass seine Fans seine Bücher auch
  in gedruckter Form kaufen als sie nur am Bildschirm zu lesen. Viele seiner Fans
  kaufen seine Bücher auch, um sie an Freundinnen und Freunde zu verschenken.
  Auf diese Weise gewinnt er mehr und mehr treue Leserinnen und Leser.
  \medskip

  \href{https://craphound.com/category/littlebrother/}{Kostprobe}
\end{noteframe}

\begin{noteframe}{Fallbeispiel Wissenschaftlerin}
  Emma fertigt ihre Doktorarbeit in Mathematik an. Sie macht das nicht nur aus
  eigenem Antrieb, weil sie Mathematik als spannend und wunderschön empfindet,
  sondern auch, um zur Gesellschaft beizutragen. Zur Finanzierung ihrer
  Promotion hält sie eine Lehrstelle inne, die von ihrer Universität und damit
  von der Öffentlichkeit getragen wird.
  \medskip

  Aus all diesen Gründen ist es für Emma selbstverständlich, ihre Arbeit unter
  \hil{CC-BY-SA} zu veröffentlichen. Das "`BY"' ist ihr wichtig,
  denn als junge Forscherin muss sie sich einen Namen machen.
  \medskip

  Emma traut sich sogar, immer wieder
  unvollständige und stellenweise peinliche Vorabversionen ihrer Arbeit auf
  ihrer Webseite zu veröffentlichen. Anstatt dass jemand ihre Ideen klaut
  (wobei sie durch ihre Veröffentlichung nachweisen könnte, sie zuerst zu
  Papier gebracht zu haben), erhält sie hilfreiche Rückmeldungen von
  Kolleginnen und Kollegen aus aller Welt, die deutlich zur Verbesserung ihrer
  Arbeit beitragen. Zudem werden so etablierte Forscher auf sie aufmerksam.
\end{noteframe}

\begin{noteframe}{Fallbeispiel Wikipedia}
  Die zu Wikipedia Beitragenden verwenden die \hil{CC-BY-SA-Lizenz}.
  Das ermöglicht Kooperationen, von denen alle profitieren:
  \medskip

  Ein Verlag entschließt sich, die Wikipedia-Artikel zu einem gewissen Thema in
  Papierform zu vertreiben. Er stellt eine Lektorin an, die die ausgewählten
  Artikel redigiert. Anschließend verkauft er seinen Artikelband im regulären
  Buchhandel.
  \medskip

  Der Verlag muss dazu nicht die Erlaubnis aller an den Artikeln Beteiligten
  einholen (eine absurde Mammutaufgabe). Er ist auch nicht verpflichtet, seinen
  Gewinn an die Wikimedia Foundation zu spenden. Wegen der "`SA"'-Klausel
  stehen die redigierten Artikel aber wieder unter derselben CC-Lizenz. Die
  Allgemeinheit hat daher das Recht, die verbesserten Artikel wieder in die
  Wikipedia einzupflegen.
  \medskip

  Die "`SA"'-Klausel stellt so sicher, dass das Gemeingut Wikipedia dauerhaft
  frei bleibt. Firmen dürfen gerne mit Wikipedia Geld verdienen, müssen aber
  der Gemeinschaft zurückgeben.
\end{noteframe}

\begin{noteframe}{Warnung vor "`NC"'-Klausel}
  Die "`NC"'-Klausel schränkt die Weiterverwendung mehr ein, als vielleicht
  beabsichtigt: Ansonsten unabhängige Blogs, die Werbung schalten, um die
  Betriebskosten zu finanzieren, gelten vermutlich auch als kommerziell.
  \medskip

  Ein besserer Schutz gegen kommerzielles Ausnutzen von Gemeingütern ohne
  Rückgabe an die Gesellschaft ist die "`SA"'-Klausel.
  \medskip

  Details stehen in einem
  \href{https://irights.info/wp-content/uploads/userfiles/CC-NC_Leitfaden_web.pdf}{Leitfaden
  von Paul Klimpel}.
\end{noteframe}

\begin{noteframe}{Fallbeispiel Internetradio}
  Eine Gruppe von Studierenden entschließt sich, ein Internetradio zu gründen.
  GEMA-Lizenzgebühren können sie sich nicht leisten. Sie haben trotzdem keine
  Mühe, ein hochqualitatives und vielfältiges musikalisches Programm zu
  gestalten, da sie auf eine Vielzahl von CC-lizenzierten Musikstücken
  zurückgreifen können.
  \medskip

  Das freut die Studierenden, die Musikerinnen und Musiker sowie die
  Zuhörenden, denn sie lernen so neue Independent-Bands abseits des
  Pop-Mainstreams kennen. Manche der Zuhörenden werden zu begeisterten Fans.
  Sie kündigen ihr Abonnement bei kommerziellen Musik-Streaming-Diensten wie
  Spotify und verteilen denselben monatlichen Betrag stattdessen über die
  Mikrozahlungsplattform Patreon direkt an ihre neuen Lieblingskünstlerinnen
  und Lieblingskünstler.
\end{noteframe}

\begin{noteframe}{Fallbeispiel Nachhaltigkeitsverein}
  Linda und Norbert schießen im Rahmen ihres Nachhaltigkeitsengagements
  zahlreiche schöne Naturaufnahmen. Diese veröffentlichen sie auf der Webseite
  ihres Vereins unter der \hil{CC-BY-SA-Lizenz}, jeweils zusammen mit
  ausführlichen Berichten zu den Vereinsaktivitäten.
  \medskip

  Nachhaltigkeitsvereine aus aller Welt können ihre Fotos für ihre eigene
  Öffentlichkeitsarbeit ebenfalls verwenden, ohne Linda und Norbert explizit um
  Erlaubnis bitten zu müssen. Immer wieder erhalten die beiden deswegen
  dankbare Mails von anderen Aktivistinnen und Aktivisten.
  \medskip

  Besonders freuen sich Linda und Norbert aber über eine erfreuliche
  Nebenwirkung: Andere Webseiten, die nichts mit Nachhaltigkeit zu tun haben
  und ganz andere Klientel ansprechen, verwenden ebenfalls ihre Bilder
  (ganz ohne Nachhaltigkeit zu thematisieren). Wegen der "`BY"'-Klausel müssen
  sie dabei die Quelle, den Nachhaltigkeitsverein der beiden, nennen. \hil{Auf diese
  Weise erreichen Linda und Norbert Menschen außerhalb ihrer Filterblase.}
\end{noteframe}

\end{document}

% LIT
% Mint
% Vorteile...
